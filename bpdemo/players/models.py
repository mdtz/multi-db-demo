from django.db import models


class Player(models.Model):
    id = models.PositiveIntegerField(primary_key=True, db_column='PLAYERID')
    first_name = models.CharField(max_length=64, db_column='FIRSTNAME')
    last_name = models.CharField(max_length=64, db_column='LASTNAME')
    key_mlbam = models.CharField(max_length=32, db_column='MLBCODE')

    class Meta:
        db_table = 'MasterPlayer_dd'
        managed = False  # tell django not to create migrations, etc for this model
