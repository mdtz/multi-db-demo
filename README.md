# multi-db demo for bp

## setup

1. clone this repo

```shell
git clone git@bitbucket.org:mdtz/multi-db-demo.git
```

2. create virtual env

```shell
cd multi-db-demo
python3 -m venv .
source bin/activate  # or windows equiv
```

3. install pip reqs

```shell
pip install -r requirements.txt
```

4. create pg database

```shell
createdb bp  # or windows or GUI equiv
```

5. configure database connections via environment variables

- `DATABASE_URL`: DSN for your postgres db from step 4, e.g., `postgresql://user:password@localhost/bp`
- `BP_WAREHOUSE_URL`: DSN for MySQL BP players db: `mysql://user:password@<host>/bp_players`

6. run django migrations

```shell
python bpdemo/manage.py migrate
```

7. start server

```shell
python bpdemo/manage.py runserver
```

8. visit api to see players list: http://localhost:8000/api/v1/players/

:)
