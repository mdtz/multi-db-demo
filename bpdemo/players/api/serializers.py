"""
Serializers define what data should be prepared
for sending back down to API consumers
"""

from rest_framework import serializers

from ..models import Player


class PlayerSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('first_name', 'last_name', 'key_mlbam')
        model = Player
