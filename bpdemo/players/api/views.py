"""Generic views in Django Rest Framework
offer a way to quickly associate streams of data
(via a queryset here) with a method for serialization
"""

from rest_framework import generics

from ..models import Player
from .serializers import PlayerSerializer


class PlayerListView(generics.ListAPIView):
    serializer_class = PlayerSerializer

    def get_queryset(self):
        """Defines a stream of data to serialize and return
        """

        # tell django to use the "bp" database when pulling players
        return Player.objects.using('bp')
